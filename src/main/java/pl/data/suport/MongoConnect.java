package pl.data.suport;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

public class MongoConnect {
    private static MongoClient mongoClient;
    private final static MongoClientURI uri =
            new MongoClientURI("mongodb://writeUser:write2vcb54@ds261644.mlab.com:61644/heroku_r4d4bfd2");

    private MongoClient getMongoClient(){
        if (mongoClient == null){
            mongoClient = new MongoClient(uri);
        }
        return mongoClient;
    }

    public MongoDatabase getDB() {
        return getMongoClient().getDatabase("heroku_r4d4bfd2");
    }

    public MongoCollection<Document> getBookCollection() {
        return getDB().getCollection("books");
    }
}
