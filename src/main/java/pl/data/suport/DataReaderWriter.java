package pl.data.suport;

import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import org.bson.Document;
import org.bson.types.ObjectId;
import pl.library.Author;
import pl.library.Book;

import java.util.ArrayList;
import java.util.List;

public class DataReaderWriter {

    MongoConnect mongo = new MongoConnect();

    List<Document> tempBooks = null;
    List<Book> books = new ArrayList<>();


    public List<Book> getBooks() {
        tempBooks = mongo.getBookCollection().find().into(new ArrayList<Document>());
        for (Document tempBook : tempBooks) {
            Book book = new Book(tempBook.getString("name"), tempBook.getInteger("year"), tempBook.getString("comment"));
            List<Document> tempAuthors = (List<Document>) tempBook.get("authors");
            for (Document tempAuthor : tempAuthors) {
                Author author = new Author(tempAuthor.getString("name"), tempAuthor.getString("surname"));
                book.addAuthor(author);
            }
            books.add(book);
        }

        return books;
    }

    public List<Book> getBooks(String query) {
        boolean copy = false;
        tempBooks = mongo.getBookCollection().find().into(new ArrayList<Document>());

        for (Document tempBook : tempBooks) {

            Book book = new Book(tempBook.getString("name"), tempBook.getInteger("year"), tempBook.getString("comment"));
            List<Document> tempAuthors = (List<Document>) tempBook.get("authors");
            for (Document tempAuthor : tempAuthors) {
                Author author = new Author(tempAuthor.getString("name"), tempAuthor.getString("surname"));
                book.addAuthor(author);
                if (author.getSurname().equalsIgnoreCase(query)) {
                    copy = true;
                }
            }
            if (copy) {
                books.add(book);
                copy = false;
            }
        }

        return books;
    }

    public void setBook(Book book) {
        Document bookDoc = new Document();
        bookDoc.put("name", book.getName());

        List<Document> authorsDoc = new ArrayList<Document>();
        for (Author item : book.getAuthors()) {
            Document author = new Document();
            author.put("name", item.getName());
            author.put("surname", item.getSurname());
            authorsDoc.add(author);
        }

        bookDoc.put("authors", authorsDoc);
        bookDoc.put("year", book.getYear());
        bookDoc.put("comment", book.getComments());
        mongo.getBookCollection().insertOne(bookDoc);

    }

    public void updateBook(String id, String newComment) {

        ObjectId objectId = new ObjectId(id);

        mongo.getBookCollection().updateOne(Filters.eq(objectId), Updates.set("comment", newComment));
    }
}
