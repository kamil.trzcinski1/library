package pl.library;

import java.util.ArrayList;
import java.util.List;

public class Book {

    private String name;
    private List<Author> authors;
    private int year;
    private String comments;

    public Book(String name, int year, String comments) {
        this.name = name;
        this.year = year;
        authors = new ArrayList<>();
        this.comments = comments;
    }

    public String getName() {
        return name;
    }

    public int getYear() {
        return year;
    }

    public String getComments() {
        return comments;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void addAuthor(Author author) {
        authors.add(author);
    }

    @Override
    public String toString() {
        return " || book title: " + name
                + " || relase date: " + year
                + " || authors: " + authorsToString()
                + " || comments: " + comments;
    }

    private String authorsToString() {
        String tempAuthors = "";
        for (Author author : authors) {
            tempAuthors += author.getName() + " " + author.getSurname() + ";";
        }
        return tempAuthors;
    }
}
