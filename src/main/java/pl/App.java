package pl;

import pl.data.suport.DataReaderWriter;
import pl.library.Author;
import pl.library.Book;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;


public class App {

    public static void main(String[] args) throws IOException {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));


        DataReaderWriter dataReader = new DataReaderWriter();

        while (true) {
            System.out.println("type book name, relase date, comment or q for exit. Use \";\" to split.");
            String[] split = reader.readLine().split(";");
            if (split[0].equalsIgnoreCase("q")) {
                break;
            }
            String comment = "";
            if (split.length == 3) {
                comment = split[2];
            }

            Book book = new Book( split[0].trim(), Integer.valueOf(split[1].trim()), comment);

            while (true) {
                System.out.print("type author name, surname or q.");
                split = reader.readLine().split(";");
                if (split[0].equalsIgnoreCase("q")) {
                    break;
                }
                Author author = new Author(split[0].trim(), split[1].trim());
                book.addAuthor(author);
            }

            DataReaderWriter dataWriter = new DataReaderWriter();
            dataWriter.setBook(book);

            List<Book> books = dataReader.getBooks();

            for (Book bookItem : books) {
                System.out.println(bookItem.toString());
            }
        }

    }
}
